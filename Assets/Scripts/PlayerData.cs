using UnityEngine;

public enum PlayerType
{
    Priest,
    Warrior,
    Rogue,
    Warlock,
    Wizard,
    Paladin,
    Sorcerer,
    Hunter,
    Druid,
    Bard,
    Artificier,
    Ranger
}

public enum GrowingState
{
    Normal,
    Growing,
    Giant,
    Decreasing,
    Cooldown
}

public enum MovingState
{
    Moving,
    Stopped
}

public enum Direction
{
    Right,
    Left
}

public enum MovementType
{
    Auto,
    Manual
}

public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    public PlayerType PlayerRole;
    public float Height = 1.00f;
    public float MaxHeight = 3.0f;
    public int[] Stats;
    public Sprite[] Movement;
    public int Position = 0;
    public float Speed;
    public float Distance;
    public float Weight;
    public float GiantSeconds;
    public float StoppedTime;
    public float GiantCooldown;
    public MovementType PlayerMovementType;
    [SerializeField]
    private Sprite _sprite;
    private SpriteRenderer _sr;
    private float _timeSinceFrameCounter;
    [SerializeField]
    private GrowingState _playerState;
    private float _growingRate;
    private MovingState _playerMovement;
    private float _clock;
    private Direction _direction;
    private float _maxX;
    private float _actualGiantSeconds;
    private float _timeSinceLastGiant;
    private float _walkedDistance;
    private float _betweenFrames;
    private int _frameRate;
    private int _rotationY;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _direction = Direction.Right;
        _clock = 0f;
        Speed = 0.1f * Weight;
        GiantSeconds = 3f;
        StoppedTime = 0.5f;
        GiantCooldown = 5f;
        transform.position = new Vector3(-5, 0, transform.position.z);
        PlayerRole = PlayerType.Druid;
        PlayerName = GameObject.Find("PlayerNewInfo").GetComponent<PlayerInputInfo>().PlayerName;
        Destroy(GameObject.Find("PlayerNewInfo"));
        _sr = GetComponent<SpriteRenderer>();
        _timeSinceFrameCounter = 0;
        _growingRate = 0.0005f;
        _frameRate = 12;
        _betweenFrames = 1f / _frameRate;
    }

    // Update is called once per frame
    void Update()
    { 
        MovementChecker();
        GrowingChecker();
    }

    public void MovementChecker()
    {
        float axisX = Input.GetAxis("Horizontal");
        if (_direction == Direction.Right) _rotationY = 180;
        else _rotationY = 0;
        transform.rotation = new Quaternion(transform.rotation.x, _rotationY, transform.rotation.z, transform.rotation.w);

        switch (_playerMovement)
        {
            case MovingState.Moving:
                IfMoving(axisX);
                break;
            case MovingState.Stopped:
                IfStopped(axisX);
                break;
        }
    }

    public void GrowingChecker()
    {
        switch (_playerState)
        {
            case GrowingState.Normal:
                if (Input.GetKeyDown(KeyCode.Space))
                    _playerState = GrowingState.Growing;
                break;
            case GrowingState.Growing:
                if (_playerMovement == MovingState.Moving)
                    Growing();
                break;
            case GrowingState.Giant:
                _actualGiantSeconds += Time.deltaTime;
                if (_actualGiantSeconds >= GiantSeconds)
                {
                    _playerState = GrowingState.Decreasing;
                    _actualGiantSeconds = 0;
                }
                break;
            case GrowingState.Decreasing:
                if (_playerMovement == MovingState.Moving)
                    Decreasing();
                break;
            case GrowingState.Cooldown:
                _timeSinceLastGiant += Time.deltaTime;
                if (_timeSinceLastGiant >= GiantCooldown)
                {
                    _playerState = GrowingState.Normal;
                    _timeSinceLastGiant = 0;
                }
                break;
        }
    }

    public void IfMoving(float axisX)
    {
        if (PlayerMovementType == MovementType.Auto)
            AutoMovement();
        else
            ManualMovement(axisX);

        if (_timeSinceFrameCounter >= _betweenFrames)
            ChangeSprite();

        _timeSinceFrameCounter += Time.deltaTime;
    }

    public void IfStopped(float axisX)
    {
        _sr.sprite = Movement[1];
        if (PlayerMovementType == MovementType.Auto)
        {
            _clock += Time.deltaTime;
            if (_clock >= StoppedTime)
                ChangeDirection();
        }

        else if (axisX != 0)
            _playerMovement = MovingState.Moving;
    }

    public void Growing()
    {
        transform.localScale = new Vector3(transform.localScale.x + _growingRate, transform.localScale.y + _growingRate, 0);
        if (_playerState == GrowingState.Growing && transform.localScale.y >= Height * 3)
        {
            _playerState = GrowingState.Giant;
        }
    }

    public void Decreasing()
    {
        transform.localScale = new Vector3(transform.localScale.x - _growingRate, transform.localScale.y - _growingRate, 0);
        if (_playerState == GrowingState.Decreasing && transform.localScale.y <= Height)
        {
            _playerState = GrowingState.Cooldown;
        }
    }

    public void ChangeDirection()
    {
        _walkedDistance = 0;
        if (_direction == Direction.Left)
            _direction = Direction.Right;
        else
            _direction = Direction.Left;

        Speed *= -1;
        _playerMovement = MovingState.Moving;
        _clock = 0f;
    }

    public void AutoMovement()
    {
        transform.position = new Vector3(Speed * Time.deltaTime + transform.position.x, transform.position.y, transform.position.z);
        _walkedDistance += Speed * Time.deltaTime;

        if (Mathf.Abs(_walkedDistance) >= Distance)
        {
            _playerMovement = MovingState.Stopped;
        }
    }

    public void ManualMovement(float axisX)
    {
        Speed = Mathf.Abs(Speed);
        transform.position = new Vector3(axisX * Speed * Time.deltaTime + transform.position.x, transform.position.y, transform.position.z);
        if (axisX == 0)
            _playerMovement = MovingState.Stopped;
        else if (axisX < 0)
        {
            _direction = Direction.Left;
            Speed *= -1;
        }
        else
            _direction = Direction.Right;
    }

    public void ChangeSprite()
    {
        _timeSinceFrameCounter = 0;
        _sr.sprite = Movement[Position];
        Position = Position < Movement.Length - 1 ? Position + 1 : 0;
    }
}
