 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    private GameObject _playerNewInfo;
    private GameObject _playerInput;
    
    // Start is called before the first frame update
    void Start()
    {
        _playerNewInfo = GameObject.Find("PlayerNewInfo");
        DontDestroyOnLoad(_playerNewInfo);
        _playerInput = GameObject.Find("PlayerName");

        /*_player = GameObject.FindWithTag("Player");
        _player.SetActive(false);

        _panel = GameObject.Find("HUDPanel");
        _panel.SetActive(false);*/
    }

    public void GameStart()
    {
        /*_player.SetActive(true);
        _panel.SetActive(true);
        gameObject.SetActive(false);*/
        var NewName = _playerInput.GetComponent<InputField>().text;
        if (!string.IsNullOrEmpty(NewName))
        {
            _playerNewInfo.GetComponent<PlayerInputInfo>().PlayerName = _playerInput.GetComponent<InputField>().text;
            SceneManager.LoadScene("GameScene");
        }
    }
}
