using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameHUDManager : MonoBehaviour
{
    private GameObject _player;
    private PlayerData _playerData;
    private Slider _speedSlider;
    private Slider _distanceSlider;
    private Slider _giantTimeSlider;
    private Dropdown _typeDropdown;
    private Dropdown _movementDropdown;
    private GameObject _nameAndType;
    private Camera _camera;
    private RectTransform _canvasRect;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _playerData = _player.GetComponent<PlayerData>();
        _speedSlider = GameObject.Find("Speed").GetComponent<Slider>();
        _distanceSlider = GameObject.Find("Distance").GetComponent<Slider>();
        _giantTimeSlider = GameObject.Find("AbilityTime").GetComponent<Slider>();
        _typeDropdown = GameObject.Find("Type").GetComponent<Dropdown>();
        _movementDropdown = GameObject.Find("MovementType").GetComponent<Dropdown>();
        _nameAndType = GameObject.Find("NameAndType");
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        _canvasRect = GameObject.Find("Canvas").GetComponent<RectTransform>();

        GameObject.Find("Name").GetComponent<Text>().text = _playerData.PlayerName;
        _speedSlider.value = _playerData.Speed;
        _distanceSlider.value = _playerData.Distance;
        _giantTimeSlider.value = _playerData.GiantSeconds;
        _nameAndType.GetComponent<Text>().text = _playerData.PlayerName + "\n" + _playerData.PlayerRole.ToString();

        var types = new List<string>(Enum.GetNames(typeof(PlayerType)));
        var movementTypes = new List<string>(Enum.GetNames(typeof(MovementType)));
        _typeDropdown.AddOptions(types);
        _movementDropdown.AddOptions(movementTypes);

        //GameObject.Find("AbilityTime").GetComponent<Slider>().value = _playerData.GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        _playerData.Speed = _playerData.Speed > 0 ? _speedSlider.value : - _speedSlider.value;
        _playerData.Distance = _distanceSlider.value;
        _playerData.GiantSeconds = _giantTimeSlider.value;
        _playerData.PlayerRole = (PlayerType)_typeDropdown.value;
        _playerData.PlayerMovementType = (MovementType)_movementDropdown.value;
        _nameAndType.GetComponent<Text>().text = _playerData.PlayerName + "\n" + _playerData.PlayerRole.ToString();
        var WorldToViewportPosition = _camera.WorldToViewportPoint(_player.transform.position);
        var PlayerScreenPosition = new Vector2((WorldToViewportPosition.x * _canvasRect.sizeDelta.x) - (_canvasRect.sizeDelta.x * 0.5f), _player.transform.localScale.y + 100);
        _nameAndType.GetComponent<RectTransform>().anchoredPosition = PlayerScreenPosition;
        GameObject.Find("FPS").GetComponent<Text>().text = "FPS: " + 1f / Time.deltaTime;
    }
}